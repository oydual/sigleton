#ifndef PERSONA_H
#define PERSONA_H
#include <QString>

class Persona
{
public:
    Persona();
    Persona(QString n,QString a,QString c) :nombre(n),apellido(a),cedula(c) {}
    QString nombre,apellido,cedula;
};

#endif // PERSONA_H
