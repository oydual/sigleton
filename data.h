#ifndef DATA_H
#define DATA_H
#include <QList>
#include <persona.h>

class Data
{
public:
    static Data *get();
    QList<Persona>& getListPersonas();
protected:
    Data();
private:
    static Data* datos;
    QList<Persona> informacion;
    static void borrar();
};

#endif // DATA_H
