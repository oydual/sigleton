#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <uno.h>


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_b_aniadir_clicked()
{
    ventana = new Uno();
    ventana->setAttribute(Qt::WA_DeleteOnClose);
    ventana->setModal(true);
    connect(ventana,&Uno::accepted,this,&MainWindow::actualizar);
    ventana->show();
}
void MainWindow::actualizar()
{
    Data *datos = Data::get();
    QList<Persona>::iterator it;
    this->ui->tablePersonas->clearContents();
    this->ui->tablePersonas->setRowCount(0);
    int i = 0;
    for (it = datos->getListPersonas().begin(); it != datos->getListPersonas().end(); ++it)
    {
        this->ui->tablePersonas->insertRow(this->ui->tablePersonas->rowCount());
        this->ui->tablePersonas->setItem(i,0,new QTableWidgetItem((*it).nombre));
        this->ui->tablePersonas->setItem(i,1,new QTableWidgetItem((*it).apellido));
        this->ui->tablePersonas->setItem(i,2,new QTableWidgetItem((*it).cedula));
        i++;
    }
}
