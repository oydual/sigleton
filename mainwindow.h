#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <uno.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_b_aniadir_clicked();
    void actualizar();

private:
    Ui::MainWindow *ui;
    Uno *ventana;
};

#endif // MAINWINDOW_H
