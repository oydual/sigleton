#include "uno.h"
#include "ui_uno.h"
#include<data.h>

Uno::Uno(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Uno)
{
    ui->setupUi(this);
}

Uno::~Uno()
{
    delete ui;
}

void Uno::on_b_ok_clicked()
{
    Data *datos;
    datos = Data::get();
    datos->getListPersonas().push_back(Persona(this->ui->l_Nombre->text(),this->ui->l_Apellido->text(),this->ui->l_Cedula->text()));
    this->accept();
}
