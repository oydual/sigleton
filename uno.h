#ifndef UNO_H
#define UNO_H

#include <QDialog>
#include <data.h>

namespace Ui {
class Uno;
}

class Uno : public QDialog
{
    Q_OBJECT

public:
    explicit Uno(QWidget *parent = 0);
    ~Uno();

private slots:
    void on_b_ok_clicked();

private:
    Ui::Uno *ui;
signals:

};

#endif // UNO_H
