#-------------------------------------------------
#
# Project created by QtCreator 2016-10-18T22:36:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Prueba
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    uno.cpp \
    data.cpp \
    persona.cpp

HEADERS  += mainwindow.h \
    uno.h \
    data.h \
    persona.h

FORMS    += mainwindow.ui \
    uno.ui
